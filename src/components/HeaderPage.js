import React from 'react';

class HeaderPage extends React.Component{
    render(){
        return (
            <div className="headerPage">
                <img src=""/>
                <ul className="menuPrincipal">
                    <li>Overview</li>
                    <li>Contacts</li>
                    <li>Favorites</li>
                </ul>
                <button>New</button>
            </div>
        );
    }
}

export default HeaderPage;